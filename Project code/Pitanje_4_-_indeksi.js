/*
4.Prikazati podatke za pet sezona (2017, 2016, 2015, 2014, 2013) za igrace koji su bili barem jednu nedelju u top 10, grupisano po njihovim godinama.
Od podataka prikazati ukupan broj meceva, poraza i pobeda za svakog ponaosob. Takodje, prikazati ukupan broj meceva i pobeda posmatrano zbirno.
*/

//db.number_of_matches_and_wins_per_season_per_surface.createIndex({"igrac":1, "sezona":1})

db.rankings_1973_2017.aggregate([
  {$match:{$expr:{$and:[{$in:["$week_year", [2013, 2014, 2015, 2016, 2017]]}, {$lte:["$rank_number", 10]}]}}},    //filtriranje dokumenata
  {$group:{"_id":{"sezona":"$week_year", "broj_godina":"$player_age"}, "igraci":{$addToSet:{"player_slug":"$player_slug"}}}}, //grupisanje po sezoni i po broju godina igraca da bi se dobili u jednom dokumentu svi igraci sa istim brojem godina
  {$unwind:"$igraci"},
  //spajanje da bismo dobili podatke o broju meceva i broju pobeda
  {$lookup:
      {
          from:"number_of_matches_and_wins_per_season_per_surface",
          let:{igrac:"$igraci.player_slug", sezona:"$_id.sezona"},
          pipeline:
          [
              {$match:{$expr:{$and:[{$eq:["$$sezona", "$sezona"]}, {$eq:["$$igrac", "$igrac"]}]}}},
              {$project:{"_id":0}}
          ],
          as:"podaci"
      },
  },
  {
      $project:
      {
          "sezona":"$_id.sezona",
          "broj_godina":"$_id.broj_godina",
          "igrac":"$igraci.player_slug",
          "rezultati":"$podaci.podaci",
          "ukupno_meceva":"$podaci.ukupno_meceva",
          "ukupno_pobeda":"$podaci.ukupno_pobeda"
      }
   },
   {
      $project:
      {
          "_id":0,
          "igrac":1,
          "broj_godina":1,
          "sezona":1,
          "rezultati":{$reduce:{input:"$rezultati", initialValue:[], in:{$concatArrays:["$$value", "$$this"]}}},
          "ukupno_meceva":{$reduce:{input:"$ukupno_meceva", initialValue:0, in:{$add:["$$value", "$$this"]}}},
          "ukupno_pobeda":{$reduce:{input:"$ukupno_pobeda", initialValue:0, in:{$add:["$$value", "$$this"]}}}
      }
   },
   {
       $project:
       {
          "igrac":1,
          "broj_godina":1,
          "sezona":1,
          "ukupno_meceva":1,
          "ukupno_pobeda":1,
          "ukupno_poraza":{$subtract:["$ukupno_meceva", "$ukupno_pobeda"]}
       }
   },
   {
       $group:
       {
           "_id":{"sezona":"$sezona", "broj_godina_igraca":"$broj_godina"}, "rezultati":{$push:{"igrac":"$igrac", 
               "ukupno_meceva":"$ukupno_meceva", "ukupno_pobeda":"$ukupno_pobeda", "ukupno_poraza":"$ukupno_poraza"}}
       }
   },
   {
       $project:
       {
           "rezultati":1,
           "ukupno_meceva_svih":{$reduce:{input:"$rezultati", initialValue:0, in:{$add:["$$value", "$$this.ukupno_meceva"]}}},
           "ukupno_pobeda_svih":{$reduce:{input:"$rezultati", initialValue:0, in:{$add:["$$value", "$$this.ukupno_pobeda"]}}}
       }
   },
   {$project:{"sezona":"$_id.sezona", "broj_godina_igraca":"$_id.broj_godina_igraca", "rezultati":1, "ukupno_meceva_svih":1, "ukupno_pobeda_svih":1}},
   {$project:{"_id":0}},
   {$sort:{"sezona":1, "broj_godina_igraca":1}}
])


