/*
7. Prikazati naziv tenisera, ukupan broj pobeda i poraza, grupisano po podlozi na kojoj se turnir igra, za svaku godinu. 
Uzeti u obzir samo one igrače koji su tokom karijere u nekoj godini imali barem 5000 poena na rang listi.
*/

db.rankings_1973_2017.aggregate(
[
    {
        $match:{$expr:{$gte:["$ranking_points", 5000]}}
    },
    {
        $group:{"_id":"$week_year", "igraci":{$addToSet:{"igrac":"$player_slug"}}} //grupisanje po sezoni da bi se dobili svi igraci koji su u nekoj sezoni imali barem 5000 poena, u okviru jednog dokumenta
    },
    {$unwind:"$igraci"}, //unwind da bi se mogao uraditi napredni lookup koji ne moze da se uradi nad nizom od vise objekata
    {
        $lookup:
        {
            from:"number_of_matches_and_wins_per_season_per_surface",
            let:{igrac:"$igraci.igrac", sezona:"$_id"},
            pipeline:
            [
                {$match:{$expr:{$and:[{$eq:["$$igrac", "$_id.igrac"]}, {$eq:["$$sezona", "$_id.sezona"]}]}}}, //filtriranje meceva na kojima je posmatrani teniser pobedio, u posmatranoj sezoni
                {$project:{"_id":0}}
            ],
            as:"podaci"
        }
    },
    {$project:
        {
            "sezona":"$_id",
            "igrac":"$igraci.igrac",
            "rezultati":"$podaci.podaci"
        }
    },
    {$project:{"_id":0, "igraci":0, "podaci":0}},
    {$sort:{"igrac":1, "sezona":1}}
])