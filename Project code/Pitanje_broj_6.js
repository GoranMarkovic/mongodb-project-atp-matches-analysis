/*
6. Prikazati top 20 tenisera od 1887 do 2017 po broju nedelja koje su proveli na prvoj poziciji na rang listi, sortirano u
opadajućem redosledu. Za svakog tenisera prikazati i ukupan broj odigranih mečeva, procenat pobeda, ukupan broj odigranih
finala, polufinala i cetvrtfinala, kao i ukupan broj pobeda u istima.
*/


db.rankings_1973_2017.aggregate([
    {$match:{"rank_number":1}}, //odabir dokumenata koji se odnose na prvu poziciju
    {$group:{"_id":"$player_slug", "broj_nedelja_na_prvom_mestu":{$sum:1}}}, //grupisanje po teniseru da bi se izracunao broj nedelja u kojima je bio prvi
    {$limit:20}, 
    {$project:{"_id":0, "igrac":"$_id", "broj_nedelja_na_prvom_mestu":1}},
    {$sort:{"broj_nedelja_na_prvom_mestu":-1}},
    {$lookup: //spajanje sa all_matches_scores a bi se dobili podaci o mecevima
        {
            from:"all_matches_scores",            
            let:{igrac:"$igrac"},
            pipeline:
            [
                {$match:{$expr:{$or:[{$eq:["$$igrac", "$winner_slug"]}, {$eq:["$$igrac", "$loser_slug"]}]}}}, //filtriranje meceva koji se odnose na posmatranog igraca
                {$addFields:{"match_year":{$substr:["$match_id",0,4]}}},
                {$facet: //sprovodjenje razlicitih proracuna nad istim ulaznim skupom podataka
                    {
                        "ukupno_meceva":
                        [
                            {$group:
                                {
                                    "_id":0, 
                                    "ukupno_meceva":{$sum:1},
                                    "winner_slug":{$push:{"winner_slug":"$winner_slug"}}
                                }
                            },
                            {$project:
                                 {
                                    "_id":0, 
                                    "ukupno_meceva":{$toInt:"$ukupno_meceva"}, 
                                    "ukupno_pobeda":
                                    {
                                        $size:
                                        {
                                            $filter:
                                            {
                                                input:"$winner_slug", 
                                                as:"winner", 
                                                cond:{$eq:["$$igrac", "$$winner.winner_slug"]}
                                            }
                                        }
                                    }
                                }
                            } 
                        ],
                        "finala":
                        [
                            {$match:{"tourney_round_name":"Finals"}},
                            {$group:{"_id":"$tourney_slug", "ukupno_meceva":{$sum:1}, "winner_slug":{$push:{"winner_slug":"$winner_slug"}}}},
                            {$project:
                                 {
                                    "_id":0,
                                    "turnir":"$_id", 
                                    "ukupno_meceva":{$toInt:"$ukupno_meceva"}, 
                                    "ukupno_pobeda":
                                    {
                                        $size:
                                        {
                                            $filter:
                                            {
                                                input:"$winner_slug", 
                                                as:"winner", 
                                                cond:{$eq:["$$igrac", "$$winner.winner_slug"]}
                                            }
                                        }
                                    }
                                }
                            },
                            {$sort:{"ukupno_meceva":-1}}
                        ],
                        "polu-finala":
                        [
                            {$match:{"tourney_round_name":"Semi-Finals"}},
                            {$group:{"_id":"$tourney_slug", "ukupno_meceva":{$sum:1}, "winner_slug":{$push:{"winner_slug":"$winner_slug"}}}},
                            {$project:
                                 {
                                    "_id":0,
                                    "turnir":"$_id", 
                                    "ukupno_meceva":{$toInt:"$ukupno_meceva"}, 
                                    "ukupno_pobeda":
                                    {
                                        $size:
                                        {
                                            $filter:
                                            {
                                                input:"$winner_slug", 
                                                as:"winner", 
                                                cond:{$eq:["$$igrac", "$$winner.winner_slug"]}
                                            }
                                        }
                                    }
                                }
                            },
                            {$sort:{"ukupno_meceva":-1}}
                        ],
                        "cetvrt-finala":
                        [  
                            {$match:{"tourney_round_name":"Quarter-Finals"}},
                            {$group:{"_id":"$tourney_slug", "ukupno_meceva":{$sum:1}, "winner_slug":{$push:{"winner_slug":"$winner_slug"}}}},
                            {$project:
                                 {
                                    "_id":0,
                                    "turnir":"$_id", 
                                    "ukupno_meceva":{$toInt:"$ukupno_meceva"}, 
                                    "ukupno_pobeda":
                                    {
                                        $size:
                                        {
                                            $filter:
                                            {
                                                input:"$winner_slug", 
                                                as:"winner", 
                                                cond:{$eq:["$$igrac", "$$winner.winner_slug"]}
                                            }
                                        }
                                    }
                                }
                            },
                            {$sort:{"ukupno_meceva":-1}}
                        ]
                    }
                 }
            ],
            as:"mecevi"
        }
    }
])