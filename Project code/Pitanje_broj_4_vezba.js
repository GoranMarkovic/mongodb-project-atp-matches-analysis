/*
4. Tenisere grupisati po godini u neku od sledećih grupa: 17-20, 21-24, 25-28, 29-32, 32-35, 36-50. Za svaku grupu prikazati
naziv tenisera koji joj pripada, broj ukupno odigranih mečeva i procenat dobijenih mečeva na nivou grupe.
*/


/*
db.rankings_1973_2017.aggregate([
  {$group:{"_id":{"sezona":"$week_year", "broj_godina":"$player_age"}, "igraci":{$addToSet:{"player_slug":"$player_slug"}}}},
  {$sort:{"_id.sezona":-1}}
])
*/


/*
db.vezba.aggregate
([
    {$lookup:
        {from:"all_matches_scores",
        localField:"igraci.player_slug",
        foreignField:"winner_slug",
        as:"pobede"
        }
    },
    {$project:
        {
            "_id":1,
            "igraci":
            {
                $map:
                {
                    input:"$igraci", as:"igrac",
                    in:
                    {
                       "pobede":{$filter:{input:"$mecevi", as:"mec", cond:{$eq:["$$mec.winner_slug", "$$igrac.player_slug"]}}},
                       "porazi":{$filter:{input:"$mecevi", as:"mec", cond:{$eq:["$$mec.loser_slug", "$$igrac.player_slug"]}}},
                    }
                }
            }
            
        }
    }
])
*/

/*
db.vezba.aggregate([
    {$facet:
        {
            "pobede":
            [
                { //first-stage:get all matches where igraci.player_slug is equal to winner_slug
                    $lookup:
                        {
                            from:"all_matches_scores",
                            localField:"igraci.player_slug",
                            foreignField:"winner_slug",
                            as:"pobede"
                        }
                },
                {  //
                }
            ],
            "porazi":
            [
                { //first-stage
                    $lookup:
                    {
                        from:"all_matches_scores",
                        localField:"igraci.player_slug",
                        foreignField:"loser_slug",
                        as:"porazi"
                    }
                }
           ] 
        }
    }
])
*/


/*
db.vezba.aggregate
([
    {$unwind:"$igraci"},
    {$lookup:
        {
            from:"all_matches_scores",
            let:{igrac:"$igraci.player_slug", sezona:"$_id.sezona"},
            pipeline:
            [
                {$match:{$expr:{$and:[{$eq:[{$toString:"$$sezona"}, {$substr:["$match_id",0,4]}]}, {$eq:["$$igrac", "$winner_slug"]}]}}},
                {$group:
                    {
                        "_id":0, 
                        "ukupno_pobeda":{$sum:1},
                        "player_slug":{$addToSet:"$winner_slug"}
                    }
                },
                {$project:{"_id":0,"player_slug":{$reduce:{input:"$player_slug", initialValue:"", in:{$concat:["$$value", "$$this"]}}}, "ukupno_pobeda":1}}
            ],
            as:"pobede"
        }
    },
    {$group:{"_id":"$_id", "pobede":{$push:"$pobede"}}},
    {$project:
        {
            "_id":1, 
            "pobede":{$reduce:{input:"$pobede", initialValue:[], in:{$concatArrays:["$$value", "$$this"]}}}
        }
    },
    {$project:
        {
            "_id":1,
            "pobede":1,
            "ukupno_pobeda":{$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value", "$$this.ukupno_pobeda"]}}}
        }
    }
])
*/

db.vezba.aggregate
([
    {$unwind:"$igraci"},
    {$lookup:
        {
            from:"all_matches_scores",
            let:{igrac:"$igraci.player_slug", sezona:"$_id.sezona"},
            pipeline:
            [
                {$match:{$expr:{$and:[{$eq:[{$toString:"$$sezona"}, {$substr:["$match_id",0,4]}]}, 
                    {$or:[{$eq:["$$igrac", "$winner_slug"]}, {$eq:["$$igrac", "$loser_slug"]}]}]}}
                },
                {$group:
                    {
                    "_id":0, 
                    "ukupno_meceva":{$sum:1},
                    "winner_slug":{$push:{"winner_slug":"$winner_slug"}}
                    }
                },
                {$project:
                    {
                        "_id":0, 
                        "ukupno_meceva":{$toInt:"$ukupno_meceva"}, 
                        "ukupno_pobeda":
                        {
                            $size:
                            {
                                $filter:
                                {
                                    input:"$winner_slug", 
                                    as:"winner", 
                                    cond:{$eq:["$$igrac", "$$winner.winner_slug"]}
                                }
                            }
                        }
                    }
                },
                {$project:{"ukupno_meceva":1, "ukupno_pobeda":1, "ukupno_poraza":{$subtract:["$ukupno_meceva", "$ukupno_pobeda"]}, "player_slug":"$$igrac"}}
             ],
            as:"mecevi"
        }
    },
    {$project:{"igraci":0}},
    {$group:{"_id":"$_id", "igrac":{$push:"$mecevi"}}},
    {$addFields:{"sezona":"$_id.sezona", "broj_godina_igraca":"$_id.broj_godina"}},
    {$project:
        {
            "_id":0, 
            "sezona":1, 
            "broj_godina_igraca":1, igrac:{$reduce:{input:"$igrac", initialValue:[], in:{$concatArrays:["$$value", "$$this"]}}}
        }
    },
    {$project:
        {
            "sezona":1,
            "broj_godina_igraca":1,
            "rezultati_igraca":"$igrac",
            "ukupno_meceva_svih":{$reduce:{input:"$igrac", initialValue:0, in:{$add:["$$value", "$$this.ukupno_meceva"]}}},
            "ukupno_pobeda_svih":{$reduce:{input:"$igrac", initialValue:0, in:{$add:["$$value", "$$this.ukupno_pobeda"]}}}
        }
    }
])
                    
                             
