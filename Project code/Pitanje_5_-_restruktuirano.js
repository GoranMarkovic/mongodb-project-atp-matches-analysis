/*
5. Prikazati za razlicite starosne grupe tenisera podatke o titulama na turnirima. Grupe treba da budu 17-20, 21-24, 25-28, 29-32, 32-35, 36-50. 
Uzeti u obzir samo one turire koji su se odigrali u godinama nakon 2014-te. Za svaku grupu prikazati tenisera i turnir koji je osvojio, godinu
odigravanja turnira kao i broj godina koje je teniser imao kada je osvojio turnir. Za svaku starosnu grupu prikazati i ukupan broj titula koje
su osvojili svi igraci koji pripadaju grupi.
*/

db.tournaments_with_winner_birth_year.aggregate([
    //grupisanje po turinru i po godini odigravanja da bi se dobio osvajac turnira u posmatranoj godini, u jednom dokumentu
    {$group:{"_id":{"turnir":"$tourney_slug", "godina_odigravanja":"$tourney_year"}, "godiste_tenisera":{$addToSet:"$godiste_osvajaca.birth_year"}, 
        "pobednici":{$push:{"pobednik":"$singles_winner_player_slug"}}}}, 
    {
        $project:
        {
            "pobednik":{$reduce:{input:"$pobednici", initialValue:"", in:{$concat:["$$value", "$$this.pobednik"]}}}, //sprovodjenje redukcije nad nizom pobednici koji se sastoji od jednog pobednika, da bi se dobilo polje "pobednik":"<naziv_tenisera>"
            "godiste":
            {
                $reduce:{input:{$reduce:{input:"$godiste_tenisera", initialValue:[], 
                in:{$concatArrays:["$$value", "$$this"]}}}, initialValue:0, in:{$add:["$$value", "$$this"]}}}
        }
    },
    {$match:{$expr:{$gte:["$_id.godina_odigravanja",2015]}}}, //odabir samo onih turnira koji su se odigrali u godinama nakon 2014-te
    {$project:{"pobednik":1, "godiste":1}},
    {$project:
        {
            "_id":0,
            "turnir":"$_id.turnir",
            "godina_odigravanja":"$_id.godina_odigravanja",
            "pobednik":1,
            "godiste":1
        }
    },
    {$project:
        {
            "turnir":1,
            "godina_odigravanja":1,
            "pobednik":1,
            "broj_godina_pobednika":{$subtract:["$godina_odigravanja", "$godiste"]}
        }
    },
    {$bucket: //grupisanje u starosne grupe
        {
            groupBy:"$broj_godina_pobednika",
            boundaries:[17, 21, 25, 29, 33, 36, 51],
            default:"Other",
            output:
            {
                "podaci":{$push:"$$ROOT"}   
            }
        }
    },
    {$sort:{"_id":1}},
    {$project:{"podaci":1, "ukupan_broj_osvajaca":{$size:"$podaci"}}},
    {$sort:{"ukupan_broj_osvajaca":-1}}
])