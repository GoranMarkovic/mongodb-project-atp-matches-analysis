/*
10. Za sledeće grupe godista: 1877-1930, 1931-1980, 1981-2017, uporediti podatke za TOP 3 tenisera za svaku od grupa 
(top 3 su ona koja imaju najbolji odnos pobeda u odnosu na broj odigranih mečeva). Podatke koje treba uporediti:
ukupno meceva, ukupno pobeda, ukupno odigranih setova i gemova, odnos pobeda u odnosu na ukupan broj odigranih meceva, za
svakog igraca ponaosob kao i za celu grupu, posmatrano zbirno. Za grupe takodje uporediti i ukupno odigranih poena
kao i ukupno dobijenih poena.
*/

db.all_matches_scores.aggregate([
    { 
        $group: //grupisanje po igracu
        {
            "_id":"$winner_slug", 
            "ukupno_pobeda":{$sum:1}, 
            "ukupno_osvojenih_setova_kao_pobednik":{$sum:"$winner_sets_won"}, 
            "ukupno_osvojenih_gemova_kao_pobednik":{$sum:"$winner_games_won"}
        }
    },
    {
        $sort:{"ukupno_pobeda":-1}
    },
    {
        $limit:500
    },
    { 
        $lookup: //spajanje sa all_matches_scores da bi se dobili podaci o porazima za nekog tenisera
        {
            from:"all_matches_scores",
            localField:"_id",
            foreignField:"loser_slug",
            as:"porazi"
        }
    },
    {
        $project:
        {
            "igrac":"$_id",
            "_id":0,
            "ukupno_pobeda":1,
            "ukupno_osvojenih_gemova_kao_pobednik":1,
            "ukupno_osvojenih_setova_kao_pobednik":1,
            "ukupno_poraza":
            {
                $reduce:
                {
                    input:"$porazi",
                    initialValue:0,
                    in:{$add:["$$value", 1]}
                }
            },
            "ukupno_osvojenih_setova_kao_gubitnik":
                {
                    $reduce:
                    {
                        input:"$porazi",
                        initialValue:0,
                        in:{$add:["$$value", "$$this.loser_sets_won"]}
                    }
                },
           "ukupno_osvojenih_gemova_kao_gubitnik":
                {
                    $reduce:
                    {
                        input:"$porazi",
                        initialValue:0,
                        in:{$add:["$$value", "$$this.loser_games_won"]}
                    }
                }
         }
    },
    {
        $project:
        {
            "igrac":1,
            "ukupno_meceva":{$add:["$ukupno_pobeda", "$ukupno_poraza"]},
            "ukupno_pobeda":1,
            "ukupno_osvojenih_setova":{$add:["$ukupno_osvojenih_setova_kao_gubitnik", "$ukupno_osvojenih_setova_kao_pobednik"]},
            "ukupno_osvojenih_gemova":{$add:["$ukupno_osvojenih_gemova_kao_gubitnik", "$ukupno_osvojenih_gemova_kao_pobednik"]},
        }
    },
    {
        $sort:{"ukupno_meceva":-1}
    },
    {
        $project:
        {
            "igrac":1,
            "ukupno_meceva":{$toInt:"$ukupno_meceva"},
            "ukupno_pobeda":{$toInt:"$ukupno_pobeda"},
            "ukupno_osvojenih_setova":1,
            "ukupno_osvojenih_gemova":1
        }
    },
    {
        $project:
        {
            "igrac":1,
            "ukupno_pobeda":1,
            "ukupno_meceva":1,
            "odnos_pobeda":{$divide:["$ukupno_pobeda", "$ukupno_meceva"]},
            "ukupno_osvojenih_setova":1,
            "ukupno_osvojenih_gemova":1
        }
    },
    {
        $sort:
        {
            "odnos_pobeda":-1
        }
    },
    {
        $lookup: //spajanje sa player_overviews da bi se dobili podaci o godistu tenisera
        {
            from:"player_overviews",
            localField:"igrac",
            foreignField:"player_slug",
            as:"karakteristike_igraca"
        }
    },
    {
        $project:
        {
            "igrac":1,
            "ukupno_meceva":1,
            "ukupno_pobeda":1,
            "odnos_pobeda":1,
            "ukupno_osvojenih_setova":1,
            "ukupno_osvojenih_gemova":1,
            "broj_godina":"$karakteristike_igraca.birth_year" 
        }
    },
    {
        $project:
        {
            "igrac":1,
            "ukupno_meceva":1,
            "ukupno_pobeda":1,
            "odnos_pobeda":1,
            "ukupno_osvojenih_setova":1,
            "ukupno_osvojenih_gemova":1,
            "broj_godina":{$reduce:{input:"$broj_godina", initialValue:0, in:{$add:["$$value", "$$this"]}}} 
        }
    },
    {
        $project:
        {
            "igrac":1,
            "ukupno_meceva":1,
            "ukupno_pobeda":1,
            "odnos_pobeda":1,
            "ukupno_osvojenih_setova":1,
            "ukupno_osvojenih_gemova":1,
            "godiste":{$toInt:"$broj_godina"} 
        }
    },
    {
        $bucket: //klasifikacija dokumenata u grupe po godistu
        {
            groupBy: "$godiste",
            boundaries:[1877, 1931, 1981, 2018],
            default:"Other",
            output:{
                mecevi:{$push:"$$ROOT"}
            }
        }
    },
    {$project:
        {
            "_id":1,
            "mecevi":{$slice:["$mecevi",3]} //odabir top 3 tenisera za svaku grupu
        }
    },
    {$project:  //racunanje ukupnog broja meceva, pobeda, odigranih setova i gemova na nivou citave grupe
        {
            "_id":1,
            "mecevi":1,
            "ukupan_broj_meceva":{$reduce:{input:"$mecevi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_meceva"]}}},
            "ukupno_pobeda":{$reduce:{input:"$mecevi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_pobeda"]}}},
            "ukupno_setova":{$reduce:{input:"$mecevi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_osvojenih_setova"]}}},
            "ukupno_gemova":{$reduce:{input:"$mecevi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_osvojenih_gemova"]}}}
        }
    },
    {$project:  
        {
            "_id":1,
            "mecevi":1,
            "ukupan_broj_meceva":{$toInt:"$ukupan_broj_meceva"},
            "ukupno_pobeda":{$toInt:"$ukupno_pobeda"},
            "ukupno_setova":{$toInt:"$ukupno_setova"},
            "ukupno_gemova":{$toInt:"$ukupno_gemova"}
        }
    },
    {$project:
        {
            "_id":1,
            "mecevi":1,
            "ukupan_broj_meceva":1,
            "ukupno_pobeda":1,
            "odnos_pobeda":{$divide:["$ukupno_pobeda", "$ukupan_broj_meceva"]},
            "ukupno_setova":1,
            "ukupno_gemova":1
        }
    },
    {
        $lookup: //spajanje sa all_matches_scores da bi se dobili svi mecevi za top 3 tenisera
        {
            from:"all_matches_scores",
            localField:"mecevi.igrac",
            foreignField:"winner_slug",
            as:"pobede"
        }
    },
    {
        $lookup:
        {
            from:"all_matches_scores",
            localField:"mecevi.igrac",
            foreignField:"loser_slug",
            as:"porazi"
        }
    },
    {$project:
        {
            "_id":1,
            "mecevi":1,
            "ukupan_broj_meceva":1,
            "ukupno_pobeda":1,
            "odnos_pobeda":1,
            "ukupno_setova":1,
            "ukupno_gemova":1,
            "pobede.match_id":1,
            "porazi.match_id":1
        }
    },
    {$project:
        {
            "_id":1,
            "mecevi":1,
            "ukupan_broj_meceva":1,
            "ukupno_pobeda":1,
            "odnos_pobeda":1,
            "ukupno_setova":1,
            "ukupno_gemova":1,
            "matches_id":{$concatArrays:["$pobede", "$porazi"]}
        }
    },
    {
        $lookup: //spajanje sa all_matches_stats da bi se dobili statisticki podaci za sve dobijene meceve
        {
            from:"all_matches_stats",
            localField:"matches_id.match_id",
            foreignField:"match_id",
            as:"statistika_meceva"
        }
    },
    {$project: //racunanje ukupnog broja odigranih i dobijenih poena na nivou grupe
        {
            "_id":1,
            "ukupan_broj_meceva":1,
            "mecevi":1,
            "ukupno_pobeda":1,
            "odnos_pobeda":1,
            "ukupno_setova":1,
            "ukupno_gemova":1,
            "winner_total_points_total":{$reduce:{input:"$statistika_meceva", initialValue:0, in:{$add:["$$value", "$$this.winner_total_points_total"]}}},
            "winner_total_points_won":{$reduce:{input:"$statistika_meceva", initialValue:0, in:{$add:["$$value", "$$this.winner_total_points_won"]}}}
         }
    } 
])
