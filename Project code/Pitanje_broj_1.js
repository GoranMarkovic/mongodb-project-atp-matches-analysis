/*
1. Prikazati pet turnira za svakog tenisera na kojima ima najvise pobeda tokom karijere. Sortirati tenisere po ukupnom broju pobeda na tih pet turnira u opadajucem
redosledu. Uzeti u obzir prvih 100. Za svaki turnir prikazati, osim naziva i broja pobeda igraca, ukupan broj titula koje je teniser osvojio na tom turniru, lokaciju
gde se odrzava turnir kao i podlogu na kojoj se igra.
*/

db.all_matches_scores.aggregate([
    //racunanje broja pobeda nekog tenisera na nekom turniru
    {$group:{"_id":{"turnir":"$tourney_slug", "teniser":"$winner_slug"}, "ukupno_pobeda":{$sum:1}}},
    //sortiranje po broju pobeda u opadajucem redosledu
    {$sort:{"ukupno_pobeda":-1}},
    //ubacivanje svih dokumenata o razlicitim turnirima koji se odnose na jednog tenisera, u jedan dokument
    {$group:{"_id":"$_id.teniser", "pobede":{$push:{"turnir":"$_id.turnir", "ukupno_pobeda":"$ukupno_pobeda"}}}},
    {$sort:{"pobede.ukupno_pobeda":-1}},
    //odabir top 5 turnira
    {$project:{"igrac":"$_id", "_id":0, "pobede":{$slice:["$pobede",5]}}},
    //racunanje ukupnog broja pobeda tenisera na top 5 turnira, zbirno posmatrano
    {$project:{"igrac":1, "top_5_turnira":"$pobede", "ukupno_pobeda":{$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value", "$$this.ukupno_pobeda"]}}}}},
    {$sort:{"ukupno_pobeda":-1}},
    //odabir 100 tenisera sa najvecim brojem pobeda na top 5 turnira, zbirno posmatrano
    {$limit:100},
    //pocetak - spajanje sa tournaments da bi se dobili podaci o turnirima
    {$unwind:"$top_5_turnira"},
    {$lookup:
        {
            from:"tournaments_1877_2017",
            let:{turnir:"$top_5_turnira.turnir", player:"$igrac"},
            pipeline:
            [
                {$match:{$expr:{$and:[{$eq:["$$turnir", "$tourney_slug"]}, {$eq:["$$player", "$singles_winner_player_slug"]}]}}},
                {$group:{"_id":"$singles_winner_name", "ukupno_titula":{$sum:1},
                     "karakteristike_turnira":{$addToSet:{"tourney_surface":"$tourney_surface", "tourney_location":"$tourney_location"}}}
                },
                {$project:{"_id":0}}
            ],
            as:"podaci_o_turniru"   
        }
    },
    //kraj - spajanje sa tournaments da bi se dobili podaci o turnirima i njihovim osvajacima
    //racunanje ukupnog broja titula
    {$project:{"igrac":1, "ukupno_pobeda":1, "turnir":{"naziv_turnira":"$top_5_turnira.turnir", "ukupno_pobeda":"$top_5_turnira.ukupno_pobeda", 
        "ukupno_titula":{$reduce:{input:"$podaci_o_turniru", initialValue:0, in:{$add:["$$value", "$$this.ukupno_titula"]}}}, 
        "karakteristike":"$podaci_o_turniru.karakteristike_turnira"}}},
    //grupisanje svih dokumenata o turnirima koji se odnose na jednog igraca u jedan dokument
    {$group:{"_id":"$igrac", "turniri":{$push:"$turnir"}, "ukupno_pobeda":{$addToSet:"$ukupno_pobeda"}}},
    {$sort:{"ukupno_pobeda":-1}}
    ])
    
    
    
    
                
                