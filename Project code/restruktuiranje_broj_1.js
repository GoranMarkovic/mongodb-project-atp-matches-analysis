//pocetak - kreiranje kolekcije tournaments_winners gde ce se nalaziti podaci o titulama svakog tenisera
/*
db.tournaments_1877_2017.aggregate([
    {
        $match:{$expr:{$ne:["$singles_winner_player_slug", null]}}
    },
    {
         $group:
         {
             "_id":{"osvajac":"$singles_winner_player_slug", "turnir":"$tourney_slug"}, "ukupno_titula":{$sum:1}
         }       
    },
    {
        $project:
        {
            "osvajac":"$_id.osvajac",
            "turnir":"$_id.turnir",
            "ukupno_titula":1
        }
    },
    {
        $project:
        {
            "_id":0
        }
    },
    {$sort:{"ukupno_titula":-1}},
    {$out:"tournaments_winners"}
]) */
//kraj - kreiranje kolekcije tournaments_winners gde ce se nalaziti podaci o ukupnom broju titula svakog tenisera

//pocetak - ugnjezdavanje kolekcija all_matches_scores i all_matches_stats; dodavanje podloge turnira i godine odigravanja meca (sablon prosirene reference)

db.all_matches_stats.createIndex({"match_id":1})
db.tournaments_1877_2017.createIndex({"tourney_year_id":1})

db.all_matches_scores.aggregate([
    {
        $lookup:
        {
            from:"tournaments_1877_2017",
            localField:"tourney_year_id",
            foreignField:"tourney_year_id",
            as:"turnir"
        }
    },
    {
        $project:
        {
            "tourney_surface":"$turnir.tourney_surface",
            "tourney_location":"$turnir.tourney_location",
             "_id" : 1, 
            "loser_games_won" : 1, 
            "loser_name" : 1, 
            "loser_player_id" : 1, 
            "loser_seed" : 1, 
            "loser_sets_won" : 1, 
            "loser_slug" : 1, 
            "loser_tiebreaks_won" : 1, 
            "match_id" : 1, 
            "match_order" : 1, 
            "match_score_tiebreaks" : 1, 
            "match_stats_url_suffix" : 1, 
            "round_order" : 1, 
            "tourney_order" : 1, 
            "tourney_round_name" : 1, 
            "tourney_slug" : 1, 
            "tourney_url_suffix" : 1, 
            "tourney_year_id" : 1, 
            "winner_games_won" : 1, 
            "winner_name" : 1, 
            "winner_player_id" : 1, 
            "winner_seed" : 1, 
            "winner_sets_won" : 1, 
            "winner_slug" : 1, 
            "winner_tiebreaks_won" : 1
        }
    },
    {
        $project:
        {
            "turnir":0
        }
    },
    {
        $project:
        {
            "tourney_surface":{$reduce:{input:"$tourney_surface", initialValue:"", in:{$concat:["$$value", "$$this"]}}} ,              
            "tourney_location":{$reduce:{input:"$tourney_location", initialValue:"", in:{$concat:["$$value", "$$this"]}}},          
             "_id" : 1, 
            "loser_games_won" : 1, 
            "loser_name" : 1, 
            "loser_player_id" : 1, 
            "loser_seed" : 1, 
            "loser_sets_won" : 1, 
            "loser_slug" : 1, 
            "loser_tiebreaks_won" : 1, 
            "match_id" : 1, 
            "match_order" : 1, 
            "match_score_tiebreaks" : 1, 
            "match_stats_url_suffix" : 1, 
            "round_order" : 1, 
            "tourney_order" : 1, 
            "tourney_round_name" : 1, 
            "tourney_slug" : 1, 
            "tourney_url_suffix" : 1, 
            "tourney_year_id" : 1, 
            "winner_games_won" : 1, 
            "winner_name" : 1, 
            "winner_player_id" : 1, 
            "winner_seed" : 1, 
            "winner_sets_won" : 1, 
            "winner_slug" : 1, 
            "winner_tiebreaks_won" : 1
        }
    },
    {
        $lookup:
        {
            from:"all_matches_stats",
            let:{mec:"$match_id"},
            pipeline:
            [
                {$match:{$expr:{$eq:["$$mec", "$match_id"]}}},
                {$project:{"_id":0}}
            ],
            as:"statistika"
        }
    },
    {$project:
        {
            "tourney_surface":1,
            "tourney_location":1,
             "_id" : 1, 
            "loser_games_won" : 1, 
            "loser_name" : 1, 
            "loser_player_id" : 1, 
            "loser_seed" : 1, 
            "loser_sets_won" : 1, 
            "loser_slug" : 1, 
            "loser_tiebreaks_won" : 1, 
            "match_id" : 1, 
            "match_order" : 1, 
            "match_score_tiebreaks" : 1, 
            "match_stats_url_suffix" : 1, 
            "round_order" : 1, 
            "tourney_order" : 1, 
            "tourney_round_name" : 1, 
            "tourney_slug" : 1, 
            "tourney_url_suffix" : 1, 
            "tourney_year_id" : 1, 
            "winner_games_won" : 1, 
            "winner_name" : 1, 
            "winner_player_id" : 1, 
            "winner_seed" : 1, 
            "winner_sets_won" : 1, 
            "winner_slug" : 1, 
            "winner_tiebreaks_won" : 1, 
            "match_year" : 1,
            "loser_aces" : "$statistika.loser_aces",
            "loser_break_points_converted" : "$statistika.loser_break_points_converted", 
            "loser_break_points_return_total" : "$statistika.loser_break_points_return_total", 
            "loser_break_points_saved" : "$statistika.loser_break_points_saved", 
            "loser_break_points_serve_total" : "$statistika.loser_break_points_serve_total", 
            "loser_double_faults" : "$statistika.loser_double_faults", 
            "loser_first_serve_points_total" : "$statistika.loser_first_serve_points_total", 
            "loser_first_serve_points_won" : "$statistika.loser_first_serve_points_won", 
            "loser_first_serve_return_total" : "$statistika.loser_first_serve_return_total", 
            "loser_first_serve_return_won" : "$statistika.loser_first_serve_return_won", 
            "loser_first_serves_in" : "$statistika.loser_first_serves_in", 
            "loser_first_serves_total" : "$statistika.loser_first_serves_total", 
            "loser_return_games_played" : "$statistika.loser_return_games_played", 
            "loser_return_points_total" : "$statistika.loser_return_points_total", 
            "loser_return_points_won" : "$statistika.loser_return_points_won", 
            "loser_second_serve_points_total" : "$statistika.loser_second_serve_points_total", 
            "loser_second_serve_points_won" : "$statistika.loser_second_serve_points_won", 
            "loser_second_serve_return_total" : "$statistika.loser_second_serve_return_total", 
            "loser_second_serve_return_won" : "$statistika.loser_aces", 
            "loser_service_games_played" : "$statistika.loser_service_games_played", 
            "loser_service_points_total" : "$statistika.loser_service_points_total", 
            "loser_service_points_won" : "$statistika.loser_service_points_won", 
            "loser_total_points_total" : "$statistika.loser_total_points_total", 
            "loser_total_points_won" : "$statistika.loser_total_points_won", 
            "match_duration" : "$statistika.match_duration",  
            //"match_stats_url_suffix" : "$statistika.match_stats_url_suffix" , 
            "match_time" : "$statistika.match_time", 
            //"tourney_order" :"$statistika.tourney_order", 
            "winner_aces" : "$statistika.winner_aces", 
            "winner_break_points_converted" : "$statistika.winner_break_points_converted", 
            "winner_break_points_return_total" : "$statistika.winner_break_points_return_total", 
            "winner_break_points_saved" : "$statistika.winner_break_points_saved", 
            "winner_break_points_serve_total" : "$statistika.winner_break_points_serve_total", 
            "winner_double_faults" : "$statistika.winner_double_faults", 
            "winner_first_serve_points_total" : "$statistika.winner_first_serve_points_total", 
            "winner_first_serve_points_won" : "$statistika.winner_first_serve_points_won", 
            "winner_first_serve_return_total" : "$statistika.winner_first_serve_return_total", 
            "winner_first_serve_return_won" : "$statistika.winner_first_serve_return_won", 
            "winner_first_serves_in" : "$statistika.winner_first_serves_in", 
            "winner_first_serves_total" : "$statistika.winner_first_serves_total", 
            "winner_return_games_played" : "$statistika.winner_return_games_played", 
            "winner_return_points_total" : "$statistika.winner_return_points_total", 
            "winner_return_points_won" : "$statistika.winner_return_points_won", 
            "winner_second_serve_points_total" : "$statistika.winner_second_serve_points_total", 
            "winner_second_serve_points_won" : "$statistika.winner_second_serve_points_won", 
            "winner_second_serve_return_total" : "$statistika.winner_second_serve_return_total", 
            "winner_second_serve_return_won" : "$statistika.winner_second_serve_return_won", 
            "winner_service_games_played" : "$statistika.winner_service_games_played", 
            "winner_service_points_total" : "$statistika.winner_service_points_total", 
            "winner_service_points_won" : "$statistika.winner_service_points_won", 
            "winner_total_points_total" : "$statistika.winner_total_points_total",
            "winner_total_points_won" : "$statistika.winner_total_points_won"
        }
    },
    {$project:{"statistika":0}},
    {$project:
        {
            "tourney_surface":1,
            "tourney_location":1,
            "turnir":1,
            "_id" : 1, 
            "loser_games_won" : 1, 
            "loser_name" : 1, 
            "loser_player_id" : 1, 
            "loser_seed" : 1, 
            "loser_sets_won" : 1, 
            "loser_slug" : 1, 
            "loser_tiebreaks_won" : 1, 
            "match_id" : 1, 
            "match_order" : 1, 
            "match_score_tiebreaks" : 1, 
            "match_stats_url_suffix" : 1, 
            "round_order" : 1, 
            "tourney_order" : 1, 
            "tourney_round_name" : 1, 
            "tourney_slug" : 1, 
            "tourney_url_suffix" : 1, 
            "tourney_year_id" : 1, 
            "winner_games_won" : 1, 
            "winner_name" : 1, 
            "winner_player_id" : 1, 
            "winner_seed" : 1, 
            "winner_sets_won" : 1, 
            "winner_slug" : 1, 
            "winner_tiebreaks_won" : 1, 
            "match_year" : 1,
            "loser_aces" : {$reduce:{input:"$loser_aces", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_break_points_converted" : {$reduce:{input:"$loser_break_points_converted", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_break_points_return_total" : {$reduce:{input:"$loser_break_points_return_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_break_points_saved" : {$reduce:{input:"$loser_break_points_saved", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_break_points_serve_total" : {$reduce:{input:"$loser_break_points_serve_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_double_faults" : {$reduce:{input:"$loser_double_faults", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_first_serve_points_total" : {$reduce:{input:"$loser_first_serve_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_first_serve_points_won" : {$reduce:{input:"$loser_first_serve_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_first_serve_return_total" : {$reduce:{input:"$loser_first_serve_return_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_first_serve_return_won" : {$reduce:{input:"$loser_first_serve_return_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_first_serves_in" : {$reduce:{input:"$loser_first_serves_in", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_first_serves_total" : {$reduce:{input:"$loser_first_serves_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_return_games_played" : {$reduce:{input:"$loser_return_games_played", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_return_points_total" : {$reduce:{input:"$loser_return_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_return_points_won" : {$reduce:{input:"$loser_return_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_second_serve_points_total" : {$reduce:{input:"$loser_second_serve_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_second_serve_points_won" : {$reduce:{input:"$loser_second_serve_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_second_serve_return_total" : {$reduce:{input:"$loser_second_serve_return_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_second_serve_return_won" : {$reduce:{input:"$loser_second_serve_return_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_service_games_played" : {$reduce:{input:"$loser_service_games_played", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_service_points_total" : {$reduce:{input:"$loser_service_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_service_points_won" : {$reduce:{input:"$loser_service_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_total_points_total" : {$reduce:{input:"$loser_total_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "loser_total_points_won" : {$reduce:{input:"$loser_total_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "match_duration" : {$reduce:{input:"$match_duration", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            //"tourney_order" :{$reduce:{input:"$tourney_order", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_aces" : {$reduce:{input:"$winner_aces", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_break_points_converted" : {$reduce:{input:"$winner_break_points_converted", initialValue:0, in:{$add:["$$value", "$$this"]}}},  
            "winner_break_points_return_total" : {$reduce:{input:"$winner_break_points_return_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_break_points_saved" : {$reduce:{input:"$winner_break_points_saved", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_break_points_serve_total" : {$reduce:{input:"$winner_break_points_serve_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_double_faults" : {$reduce:{input:"$winner_double_faults", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_first_serve_points_total" : {$reduce:{input:"$winner_first_serve_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_first_serve_points_won" : {$reduce:{input:"$winner_first_serve_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_first_serve_return_total" : {$reduce:{input:"$winner_first_serve_return_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_first_serve_return_won" : {$reduce:{input:"$winner_first_serve_return_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_first_serves_in" : {$reduce:{input:"$winner_first_serves_in", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_first_serves_total" : {$reduce:{input:"$winner_first_serves_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_return_games_played" : {$reduce:{input:"$winner_return_games_played", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_return_points_total" : {$reduce:{input:"$winner_return_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_return_points_won" : {$reduce:{input:"$winner_return_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_second_serve_points_total" : {$reduce:{input:"$winner_second_serve_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_second_serve_points_won" : {$reduce:{input:"$winner_second_serve_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_second_serve_return_total" : {$reduce:{input:"$winner_second_serve_return_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_second_serve_return_won" : {$reduce:{input:"$winner_second_serve_return_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_service_games_played" : {$reduce:{input:"$winner_service_games_played", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_service_points_total" : {$reduce:{input:"$winner_service_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_service_points_won" : {$reduce:{input:"$winner_service_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_total_points_total" : {$reduce:{input:"$winner_total_points_total", initialValue:0, in:{$add:["$$value", "$$this"]}}}, 
            "winner_total_points_won" : {$reduce:{input:"$winner_total_points_won", initialValue:0, in:{$add:["$$value", "$$this"]}}}
         }
    },
    {$addFields:{"match_year":{$toInt:{$substr:["$tourney_year_id", 0, 4]}}}},
    {$out:"all_matches_scores_and_stats"}
]) 

//db.player_overviews.createIndex({"player_slug":1})
//kraj - ugnjezdavanje kolekcija all_matches_scores i all_matches_stats; dodavanje podloge turnira (sablon prosirene reference)

//pocetak - ubacivanje godista u all_matches_scores, sablon prosirene reference
db.player_overviews.createIndex({"player_slug":1})
/*
db.tournaments_1877_2017.aggregate([
    {
        $lookup:
        {
            from: "player_overviews",
            let:{igrac: "$singles_winner_player_slug"},
            pipeline:
            [
                {$match:{$expr:{$eq:["$$igrac", "$player_slug"]}}},
                {$project:{"_id":0, "birth_year":1}}
            ],
            as: "godiste_osvajaca"
          
        }
    },
    {$out:"tournaments_with_winner_birth_year"}
]) */
//kraj - ubacivanje godista u all_matches_scores, sablon prosirene reference

db.all_matches_scores_and_stats.createIndex({"winner_slug":1})
db.all_matches_scores_and_stats.createIndex({"loser_slug":1})

db.all_matches_scores_and_stats.aggregate
([
    {$group:
        {
            "_id":{"igrac":"$winner_slug", "sezona":"$match_year", "podloga":"$tourney_surface"}, "ukupno_pobeda":{$sum:1}
        }
    },
    //{$match:{"_id.igrac":"novak-djokovic"}},
    {$sort:{"ukupno_pobeda":-1}},
    {$lookup:
        {
            from:"all_matches_scores_and_stats",
            let:{igrac:"$_id.igrac", podloga:"$_id.podloga", sezona:"$_id.sezona"},
            pipeline:
            [
                {$match:{$expr:{$and:[{$eq:["$$igrac", "$loser_slug"]}, {$eq:["$$podloga", "$tourney_surface"]}, {$eq:["$$sezona", "$match_year"]}]}}},
                {$group:
                    {
                        "_id":{"sezona":"$match_year", "podloga":"$podloga_podaci.tourney_surface"}, "ukupno_poraza":{$sum:1}
                    }
                },
                {$project:{"_id":0}}
            ],
            as:"porazi"
        }
    },
    {$project:{"ukupno_pobeda":1, "ukupno_poraza":"$porazi.ukupno_poraza", "podloga":1}},
    {$project:{"igrac":"$_id.igrac", "sezona":"$_id.sezona", "podloga":"$_id.podloga", 
        "ukupno_pobeda":{$toInt:"$ukupno_pobeda"}, "ukupno_poraza":{$toInt:{$reduce:{input:"$ukupno_poraza", initialValue:0, in:{$add:["$$value", "$$this"]}}}}}},
    {$project:{"_id":0}},
    {$project:{"igrac":1, "sezona":1, "podloga":1, "ukupno_pobeda":1, "ukupno_poraza":1, "ukupno_meceva":{$add:["$ukupno_pobeda", "$ukupno_poraza"]}}},
    {$group:{"_id":{"igrac":"$igrac", "sezona":"$sezona"}, 
        "podaci":{$push:{"podloga":"$podloga", "ukupno_meceva":"$ukupno_meceva", "ukupno_pobeda":"$ukupno_pobeda"}}}},
    {$project:{"igrac":"$_id.igrac", "sezona":"$_id.sezona","podaci":1}},
    {$project:{"_id":0}},
    {$project:
        {
            "igrac":1, "sezona":1, "podaci":1, 
            "ukupno_meceva":{$reduce:{input:"$podaci", initialValue:0, in:{$add:["$$value", "$$this.ukupno_meceva"]}}},
            "ukupno_pobeda":{$reduce:{input:"$podaci", initialValue:0, in:{$add:["$$value", "$$this.ukupno_pobeda"]}}}
        }
    },
    {
        $lookup:
        {
            from:"all_matches_scores_and_stats",
            let:{igrac:"$igrac"},
            pipeline:
            [
                {$match:{$expr:{$or:[{$eq:["$$igrac", "$winner_slug"]}, {$eq:["$$igrac", "$loser_slug"]}]}}}
            ],
            as:"mecevi"
        }
    },
    {
        $project:
        {
            "igrac":1,
            "ukupno_meceva":1,
            "ukupno_pobeda":1,
            "sezona":1,
            "podaci":1,
            "pobede":{$filter:{input:"$mecevi", as:"mec", cond:{$eq:["$$mec.winner_slug", "$igrac"]}}},
            "porazi":{$filter:{input:"$mecevi", as:"mec", cond:{$eq:["$$mec.loser_slug", "$igrac"]}}}
        }
    },
    {$project:
        {
            "igrac":1,
            "ukupno_meceva":1,
            "ukupno_pobeda":1,
            "sezona":1,
            "podaci":1,
            "winner_total_points_total":{$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value", "$$this.winner_total_points_total"]}}},
            "winner_total_points_won":{$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value", "$$this.winner_total_points_won"]}}},
            "loser_total_points_total":{$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value", "$$this.loser_total_points_total"]}}},
            "loser_total_points_won":{$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value", "$$this.loser_total_points_won"]}}},
            "winner_sets_won":{$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value", "$$this.winner_sets_won"]}}},
            "winner_games_won":{$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value", "$$this.winner_games_won"]}}},
            "loser_sets_won":{$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value", "$$this.loser_sets_won"]}}},
            "loser_games_won":{$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value", "$$this.loser_games_won"]}}}
        }
    },
    {
        $project:
        {
             "igrac":1,
            "ukupno_meceva":1,
            "ukupno_pobeda":1,
            "sezona":1,
            "podaci":1,
            "ukupno_odigranih_poena":"$winner_total_points_total",
            "ukupno_dobijenih_poena":{$add:["$winner_total_points_won", "$loser_total_points_won"]},
            "ukupno_odigranih_setova":{$add:["$winner_sets_won", "$loser_sets_won"]},
            "ukupno_odigranih_gemova":{$add:["$winner_games_won", "$loser_games_won"]}
            
        }
    },
    {$out:"number_of_matches_and_wins_per_season_per_surface"} 
]) 



//db.all_matches_scores_and_stats.createIndex({"loser_slug":1, "podloga_podaci.tourney_surface":1, "match_year":1})


//db.all_matches_scores_and_stats.find({"winner_slug":"novak-djokovic"})



db.all_matches_scores_and_stats.aggregate([
    {
        $group:
        {
            "_id":"$winner_slug", 
            "ukupno_pobeda":{$sum:1}, 
            "ukupno_osvojenih_setova_pobednik":{$sum:"$winner_sets_won"},
            "ukupno_osvojenih_gemova_pobednik":{$sum:"$winner_games_won"},
            "ukupno_odigranih_poena":{$sum:"$winner_total_points_total"},
            "ukupno_osvojenih_poena_pobednik":{$sum:"$winner_total_points_won"}
        }
    },
    {
        $lookup:
        {
            from:"all_matches_scores_and_stats",
            let:{igrac:"$_id"},
            pipeline:
            [
                {$match:{$expr:{$eq:["$$igrac", "$loser_slug"]}}},
                {
                    $group:
                    {
                        "_id":0,
                        "ukupno_poraza":{$sum:1}, 
                        "ukupno_osvojenih_setova_gubitnik":{$sum:"$loser_sets_won"},
                        "ukupno_osvojenih_gemova_gubitnik":{$sum:"loser_games_won"},
                        "ukupno_osvojenih_poena_gubitnik":{$sum:"$loser_total_points_won"}
                    }
                }
            ],
            as:"porazi"
        }
    },
    {
        $project:
        {
            "_id":1,
            "ukupno_pobeda":1,
            "ukupno_meceva":{$add:["$ukupno_pobeda", {$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_poraza"]}}}]},
            "ukupno_osvojenih_setova":{$add:["$ukupno_osvojenih_setova_pobednik", {$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_osvojenih_setova_gubitnik"]}}}]},
            "ukupno_osvojenih_gemova":{$add:["$ukupno_osvojenih_gemova_pobednik", {$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_osvojenih_gemova_gubitnik"]}}}]},
            "ukupno_odigranih_poena":1,
            "ukupno_osvojenih_poena":{$add:["$ukupno_osvojenih_poena_pobednik", {$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_osvojenih_poena_gubitnik"]}}}]}
        }
    },
    {
        $project:
        {
             "_id":1,
             "ukupno_meceva":1,
             "ukupno_pobeda":1,
             "odnos_pobeda":{$divide:[{$toInt:"$ukupno_pobeda"}, {$toInt:"$ukupno_meceva"}]},
             "ukupno_osvojenih_setova":1,
             "ukupno_osvojenih_gemova":1,
             "ukupno_odigranih_poena":1,
             "ukupno_osvojenih_poena":1   
        }
    },
    {$sort:{"ukupno_meceva":-1}}
])
