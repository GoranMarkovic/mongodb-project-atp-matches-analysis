/*
4.Prikazati podatke za pet sezona (2017, 2016, 2015, 2014, 2013) za igrace koji su bili barem jednu nedelju u top 10, grupisano po njihovim godinama.
Od podataka prikazati ukupan broj meceva, poraza i pobeda za svakog ponaosob. Takodje, prikazati ukupan broj meceva i pobeda posmatrano zbirno.
*/



db.rankings_1973_2017.aggregate([
  {$match:{$expr:{$and:[{$in:["$week_year", [2013, 2014, 2015, 2016, 2017]]}, {$lte:["$rank_number", 10]}]}}},    //filtriranje dokumenata
  {$group:{"_id":{"sezona":"$week_year", "broj_godina":"$player_age"}, "igraci":{$addToSet:{"player_slug":"$player_slug"}}}}, //grupisanje po sezoni i po broju godina igraca da bi se dobili u jednom dokumentu svi igraci sa istim brojem godina
  {$unwind:"$igraci"}, //unwind da bi se mogao uraditi napredni lookup koji ne funkcionise ukoliko u nizu ima vise od jednog objekta
    {$lookup: //spajanje sa all_matches_scores da bi se dobili podaci o ukupnom broju meceva, poraza i pobeda
        {
            from:"all_matches_scores",
            let:{igrac:"$igraci.player_slug", sezona:"$_id.sezona"},
            pipeline:
            [
                {$match:{$expr:{$and:[{$eq:[{$toString:"$$sezona"}, {$substr:["$match_id",0,4]}]}, 
                    {$or:[{$eq:["$$igrac", "$winner_slug"]}, {$eq:["$$igrac", "$loser_slug"]}]}]}}
                },
                {$group:
                    {
                    "_id":0, 
                    "ukupno_meceva":{$sum:1},
                    "winner_slug":{$push:{"winner_slug":"$winner_slug"}}
                    }
                },
                {$project:
                    {
                        "_id":0, 
                        "ukupno_meceva":{$toInt:"$ukupno_meceva"}, 
                        "ukupno_pobeda":
                        {
                            $size:
                            {
                                $filter:
                                {
                                    input:"$winner_slug", 
                                    as:"winner", 
                                    cond:{$eq:["$$igrac", "$$winner.winner_slug"]}
                                }
                            }
                        }
                    }
                },
                {$project:{"ukupno_meceva":1, "ukupno_pobeda":1, "ukupno_poraza":{$subtract:["$ukupno_meceva", "$ukupno_pobeda"]}, "player_slug":"$$igrac"}}
             ],
            as:"mecevi"
        }
    },
    {$project:{"igraci":0}},
    {$group:{"_id":"$_id", "igrac":{$push:"$mecevi"}}},  //grupisanje po sezoni i broju godina da bi se u jednom dokumentu nalazi dokumenti o teniseru (ukupno meceva, pobeda i proaza)
    {$addFields:{"sezona":"$_id.sezona", "broj_godina_igraca":"$_id.broj_godina"}},
    {$project:
        {
            "_id":0, 
            "sezona":1, 
            "broj_godina_igraca":1, igrac:{$reduce:{input:"$igrac", initialValue:[], in:{$concatArrays:["$$value", "$$this"]}}} 
        }
    },
    {$project:
        {
            "sezona":1,
            "broj_godina_igraca":1,
            "rezultati_igraca":"$igrac",
            "ukupno_meceva_svih":{$reduce:{input:"$igrac", initialValue:0, in:{$add:["$$value", "$$this.ukupno_meceva"]}}},
            "ukupno_pobeda_svih":{$reduce:{input:"$igrac", initialValue:0, in:{$add:["$$value", "$$this.ukupno_pobeda"]}}}
        }
    },
    {$sort:{"sezona":1, "broj_godina_igraca":1}}
])                    
