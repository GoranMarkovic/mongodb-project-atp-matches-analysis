/*
7. Prikazati naziv tenisera, ukupan broj pobeda i poraza, grupisano po podlozi na kojoj se turnir igra, za svaku godinu. 
Uzeti u obzir samo one igrače koji su tokom karijere u nekoj godini imali barem 5000 poena na rang listi.
*/

db.rankings_1973_2017.aggregate(
[
    {
        $match:{$expr:{$gte:["$ranking_points", 5000]}}
    },
    {
        $group:{"_id":"$week_year", "igraci":{$addToSet:{"igrac":"$player_slug"}}} //grupisanje po sezoni da bi se dobili svi igraci koji su u nekoj sezoni imali barem 5000 poena, u okviru jednog dokumenta
    },
    {$unwind:"$igraci"}, //unwind da bi se mogao uraditi napredni lookup koji ne moze da se uradi nad nizom od vise objekata
    {$lookup: //spajanje sa all_matches_scores da bi se dobili podaci o pobedama i porazima
        {
            from:"all_matches_scores",
            let:{igrac:"$igraci.igrac", sezona:"$_id"},
            pipeline:
            [
                {$match:{$expr:{$and:[{$eq:["$$igrac", "$winner_slug"]}, {$eq:[{$toString:"$$sezona"}, {$substr:["$tourney_year_id", 0, 4]}]}]}}}, //filtriranje meceva na kojima je posmatrani teniser pobedio, u posmatranoj sezoni
                {$lookup: //spajanje sa tournaments da bi se dobio podatak o podlozi na kojoj se odigrava turnir
                    {
                        from:"tournaments_1877_2017",
                        localField:"tourney_year_id",
                        foreignField:"tourney_year_id",
                        as:"turnir"
                    }
                },
                {$project:{"_id":0, "tourney_slug":1, "turnir.tourney_surface":1}},
                {$project:{"tourney_slug":1, "tourney_surface":{$reduce:{input:"$turnir", initialValue:"", in:{$concat:["$$value", "$$this.tourney_surface"]}}}}},
                {$group:{"_id":"$tourney_surface", "ukupno_pobeda":{$sum:1}}},
                {$sort:{"ukupno_pobeda":-1}}
             ],
            as:"pobede"
        }
    },
    {$lookup:
        {
            from:"all_matches_scores",
            let:{igrac:"$igraci.igrac", sezona:"$_id"},
            pipeline:
            [
                {$match:{$expr:{$and:[{$eq:["$$igrac", "$loser_slug"]}, {$eq:[{$toString:"$$sezona"}, {$substr:["$tourney_year_id", 0, 4]}]}]}}},
                {$lookup:
                    {
                        from:"tournaments_1877_2017",
                        localField:"tourney_year_id",
                        foreignField:"tourney_year_id",
                        as:"turnir"
                    }
                },
                {$project:{"_id":0, "tourney_slug":1, "turnir.tourney_surface":1}},
                {$project:{"tourney_slug":1, "tourney_surface":{$reduce:{input:"$turnir", initialValue:"", in:{$concat:["$$value", "$$this.tourney_surface"]}}}}},
                {$group:{"_id":"$tourney_surface", "ukupno_poraza":{$sum:1}}},
                {$sort:{"ukupno_poraza":-1}}
             ],
            as:"porazi"
        }
    },
    {$sort:{"_id":-1}}
])