/*
2. Za igrace koji su tokom neke sezone imali promenu pozicije na rang listi, od barem 200 mesta, prikazati sledece podatke: sezona, ime, prezime, broj godina u toj sezoni,
tezinu, visinu, ukupan broj odigranih meceva, procenat pobeda.
*/

//db.getProfilingStatus()
//db.setProfilingLevel(0)

db.rankings_1973_2017.aggregate([
    {$group:{"_id":{"Godina":"$week_year", "Igrac":"$player_slug"},  //grupisanje po sezoni i po igracu
        "srednja_vrednost_pozicije":{$avg:"$rank_number"},          
        "standardna_devijacija":{$stdDevPop:"$rank_number"}}},
    {$match:{$expr:{$gte:["$standardna_devijacija", 150]}}}, //odbacivanje onih koji nemaju standardnu devijaciju vecu od 150 da bi se dobili svi koji imaju pomeraj od barem 200
    {$lookup:  //spajanje sa player_overviews da bi se dobili podaci o igracu
        {
            from: "player_overviews",
            let:{player_slug:"$_id.Igrac"},
            pipeline:
            [
                { $match:
                        { $expr:
                               { 
                                   $eq: ["$player_slug", "$$player_slug"]
                               }
                        }
                }
            ],
            as:"karakteristike_igraca"
        }
    },
    {$lookup: //spajanje sa all_matches_scores da bi se dobili svi mecevi igraca
        {
            from: "all_matches_scores",
            let:{player_slug:"$_id.Igrac", sezona:"$id_Godina"},
            pipeline:
            [
                { $match:
                        { $expr:
                               {$and:[{$or:[{$eq: ["$$player_slug", "$winner_slug"]},{$eq: ["$$player_slug", "$loser_slug"]}]}, {$eq:["$$sezona", {$substr:["$tourney_year_id",0,4]}]}]}
                        }
                }
             ],
            as:"mecevi_igraca"
        }
    },
    {$project:
        {
            "_id":1, 
            "srednja_vrednost_pozicije":1, 
            "standardna_devijacija":1, 
            "karakteristike_igraca":1, 
            "mecevi_igraca":
            {
                 $filter: //odbacivanje svih meceva koji se ne odnose na posmatranu sezonu
                 {
                    input:"$mecevi_igraca", 
                    as: "mec", 
                    cond:
                    {
                        $eq:[ {$substr:["$$mec.tourney_year_id",0,4]}, {$toString:"$_id.Godina"} ]
                    }
                 }
            }
        }
    },
    {$set:{"ukupno_meceva":0, "ukupno_pobeda":0, "ukupno_poraza":0}},
    {$project:{
        "_id":1, "karakteritike_igraca":1, 
        "mecevi_igraca.loser_slug":1, "mecevi_igraca.winner_slug":1, 
        "ukupno_meceva":{$size:"$mecevi_igraca"}, 
        "ukupno_pobeda":1, "ukupno_poraza":1}},
    {$unwind:"$mecevi_igraca"}
])

