/*
10. Za sledeće grupe godista: 1877-1930, 1931-1980, 1981-2017, uporediti podatke za TOP 3 tenisera za svaku od grupa 
(top 3 su ona koja imaju najbolji odnos pobeda u odnosu na broj odigranih mečeva). Podatke koje treba uporediti:
ukupno meceva, ukupno pobeda, ukupno odigranih setova i gemova, odnos pobeda u odnosu na ukupan broj odigranih meceva, za
svakog igraca ponaosob kao i za celu grupu, posmatrano zbirno. Za grupe takodje uporediti i ukupno odigranih poena
kao i ukupno dobijenih poena.
*/

db.players_all_seasons_statistics.aggregate(
[
    {
        $lookup:
        {
            from:"player_overviews",
            let:{igrac:"$_id"},
            pipeline:
            [
                {$match:{$expr:{$eq:["$$igrac","$player_slug"]}}},
                {$project:{"_id":0, "birth_year":1}}
            ],
            as:"godiste"
        }
    },
    {
        $project:
        {
            "_id":1,
            "ukupno_pobeda":1,
            "ukupno_odigranih_poena":1,
            "ukupno_meceva":1,
            "ukupno_osvojenih_setova":1,
            "ukupno_osvojenih_gemova":1,
            "ukupno_osvojenih_poena":1,
            "odnos_pobeda":1,
            "godiste":{$reduce:{input:"$godiste.birth_year", initialValue:0, in:{$add:["$$value", "$$this"]}}}
        }
    },
    {
        $bucket: //klasifikacija dokumenata u grupe po godistu
        {
            groupBy: "$godiste",
            boundaries:[1877, 1931, 1981, 2018],
            default:"Other",
            output:{
                mecevi:{$push:"$$ROOT"}
            }
        }
    },
    {$project:
        {
            "_id":1,
            "mecevi":{$slice:["$mecevi",3]} //odabir top 3 tenisera za svaku grupu
        }
    },
    {$project:  //racunanje ukupnog broja meceva, pobeda, odigranih setova i gemova na nivou citave grupe
        {
            "_id":1,
            "mecevi":1,
            "ukupan_broj_meceva":{$reduce:{input:"$mecevi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_meceva"]}}},
            "ukupno_pobeda":{$reduce:{input:"$mecevi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_pobeda"]}}},
            "ukupno_setova":{$reduce:{input:"$mecevi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_osvojenih_setova"]}}},
            "ukupno_gemova":{$reduce:{input:"$mecevi", initialValue:0, in:{$add:["$$value", "$$this.ukupno_osvojenih_gemova"]}}}
        }
    }
])