/*
2. Prikazati podatke za najstandardnije tenisere u istoriji. Idealno standardan je onaj koji je tokom citave karijere zadrzao prvu poziciju 
(solidno standardan je npr. onaj koji se tokom citave karijere kretao oko pete pozicije). U obzir uzeti podatke za poziciju na rang listi 
samo u onim nedeljama u kojima je igrac bio u prvih 50. Podatke koje treba prikazati: kretnja pozicije kroz sezone, ukupno meceva i pobeda kroz sezone, 
ukupno meceva i pobeda posmatrano tokom cele karijere, datum rodjenja igraca.
*/

db.rankings_1973_2017.aggregate([
    {$match:{"rank_number":{$lte:50}}},    //uzimanje u obzir samo onih dokumenata koji se odnose na nedelju tenisera u kojoj je na rang listi bio u prvih 50 pozicija
    {$group:{"_id":{"Godina":"$week_year", "Igrac":"$player_slug"},    //grupisanje po sezoni i po igracu 
        "srednja_vrednost_pozicije":{$avg:"$rank_number"}, 
        "standardna_devijacija":{$stdDevPop:"$rank_number"}}},
    {$sort:{"srednja_vrednost_pozicije":1, "standardna_devijacija":1}},    //sortiranje po najboljim vrednostima u opadajucem redosledu
    //pocetak - racunanje srednje vrednosti pozicije i standardne devijacije, posmatrano zbirno, za celu karijeru    
    {$group:
        {"_id":"$_id.Igrac", 
            "srednja_vrednost_pozicije":{$avg:"$srednja_vrednost_pozicije"},
            "standardna_devijacija":{$avg:"$standardna_devijacija"},
            "sezone":
            {
                $push:
                {
                    "Godina":"$_id.Godina", 
                    "srednja_vrednost_pozicije":"$srednja_vrednost_pozicije", 
                    "standardna_devijacija":"$standardna_devijacija"
                }
            }
        }
    },
    //kraj - racunanje srednje vrednosti pozicije i standardne devijacije, posmatrano zbirno, za celu karijeru 
    {$sort:{"srednja_vrednost_pozicije":1, "standardna_devijacija":1}},
    {$lookup:
        {from:"player_overviews",
        let:{player_id:"$_id"},
        pipeline:
        [
            {$match:{$expr:{$eq:["$$player_id", "$player_slug"]}}},
            {$project:{"_id":0, "rodjen":"$birth_year"}}
        ],
        as:"karakteristike_igraca"
        }
    },
    //spajanje da bismo dobili podatke o svim mecevima i pobedama po sezoni kao i tokom citave karijere
    {
        $lookup:
        {
            from:"number_of_matches_and_wins_per_season_per_surface",
            localField:"_id",
            foreignField:"igrac",
            as:"podaci"
        }
    },
    {$project:
        {
             "podaci._id":0,
             "podaci.igrac":0
        }
    },
    {
        $project:
        {
            "godiste":{$reduce:{input:"$karakteristike_igraca", initialValue:0, in:{$add:["$$value", "$$this.rodjen"]}}},
            "_id":1,
            "srednja_vrednost_pozicije":1,
            "standardna_devijacija":1,
            "sezone":1,
            "podaci":1,
            "ukupno_meceva_tokom_karijere":{$reduce:{input:"$podaci", initialValue:0, in:{$add:["$$value", "$$this.ukupno_meceva"]}}},
            "ukupno_pobeda_tokom_karijere":{$reduce:{input:"$podaci", initialValue:0, in:{$add:["$$value", "$$this.ukupno_pobeda"]}}}
        }
    }
])