/*
3. Prikazati trend kretanja statističkih parametara za tenisere, grupisano po sezonama. 
Uzeti u obzir sledeće parametre: broj aseva, broj duplih gresaka, broj izvedenih prvih serviranja, broj uspesnih prvih serviranja, ukupno odigranih poenta,
ukupno osvojenih poena, ukupan broj meceva, ukupno pobeda i odnos pobeda.
Osim njih prikazati i sledeće podatke o teniseru: broj godina, najnižu i najvišu poziciju na rang listi u posmatranoj godini. 
Uzeti u obzir samo one igrače koji su u toku posmatrane godine bili makar jedanput među top 10 na rang listi.
*/

db.rankings_1973_2017.createIndex({"_id.teniser":1})
db.rankings_1973_2017.createIndex({"_id.sezona":1})


db.rankings_1973_2017.aggregate([
    {  
        $group:  //grupisanje po sezoni i po teniseru
        {
            "_id":
            {
                "sezona":"$week_year", "teniser":"$player_slug"
            }, 
            "najbolja_pozicija":{$min:"$rank_number"}, 
            "najgora_pozicija":{$max:"$rank_number"},
            "player_age":{$addToSet:"$player_age"}
        }
    },
    {
        $match: //odbacivanje onih tenisera koji nisu u posmatranoj sezoni bili nijednom u top 10
        {
            $expr:{$lte:["$najgora_pozicija", 10]}
        }
    },
    {$lookup: //spajanje tenisera sa svim mecevima u kojima je ucestvovao u posmatranoj sezoni
        {
            from:"all_matches_scores_and_stats",
            let:{player_id:"$_id.teniser", sezona:"$_id.sezona"},
            pipeline:
            [
                {$match:
                    {$expr:
                        {$and:
                            [
                                {$or:[{$eq:["$$player_id", "$winner_slug"]}, {$eq:["$$player_id", "$loser_slug"]}]},
                                {$eq:["$$sezona", {$toInt:{$substr:["$tourney_year_id", 0, 4]}}]}
                            ]
                        }
                     }
                 }
            ],
            as:"mecevi"
        }
    },
    {$project: 
        {
            "najbolja_pozicija":1,
            "najgora_pozicija":1,
            "player_age":1,
            //filtriranje niza svih meceva na niz meceva u kojima je teniser pobedio i na niz meceva u kojima je teniser izgubio
            "pobede":{$filter:{input:"$mecevi", as:"mec", cond:{$eq:["$$mec.winner_slug", "$_id.teniser"]}}},
            "porazi":{$filter:{input:"$mecevi", as:"mec", cond:{$eq:["$$mec.loser_slug", "$_id.teniser"]}}}
        }
    },
    {$project:
        {
            "najbolja_pozicija":1,
            "najgora_pozicija":1,
            "player_age":1,
            //racunanje ukupnog broja pobeda i poraza
            "broj_pobeda":{$size:"$pobede"},
            "broj_poraza":{$size:"$porazi"},
            "osvojeni_asovi" : 
            {
                $add:
                [
                    {$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value","$$this.winner_aces"]}}}, 
                    {$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value","$$this.loser_aces"]}}}
                ]
            }, 
            "duple_greske" : 
            {
                $add:
                [
                    {$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value","$$this.winner_double_faults"]}}}, 
                    {$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value","$$this.loser_double_faults"]}}}
                ]
            },
            "uspesna_prva_serviranja" : 
            {
                $add:
                [
                    {$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value","$$this.winner_first_serves_in"]}}}, 
                    {$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value","$$this.loser_first_serves_in"]}}}
                ]
            }, 
            "prva_serviranja" : 
            {
                $add:
                [
                    {$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value","$$this.winner_first_serves_total"]}}}, 
                    {$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value","$$this.loser_first_serves_total"]}}}
                ]
            },
            "ukupno_odigranih_poena" : 
            {
                $add:
                [
                    {$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value","$$this.winner_total_points_total"]}}}, 
                    {$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value","$$this.loser_total_points_total"]}}}
                ]
            },
            "ukupno_osvojenih_poena" : 
            {
                $add:
                [
                    {$reduce:{input:"$pobede", initialValue:0, in:{$add:["$$value","$$this.winner_total_points_won"]}}}, 
                    {$reduce:{input:"$porazi", initialValue:0, in:{$add:["$$value","$$this.loser_total_points_won"]}}}
                ]
            }  
         }
     } 
])