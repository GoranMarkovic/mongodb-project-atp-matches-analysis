/*
2. Prikazati podatke za najstandardnije tenisere u istoriji. Idealno standardan je onaj koji je tokom citave karijere zadrzao prvu poziciju (solidno standardan je
npr. onaj koji se tokom citave karijere kretao oko pete pozicije). U obzir uzeti podatke za poziciju na rang listi samo u onim nedeljama u kojima je igrac bio u 
prvih 50. Podatke koje treba prikazati: kretnja pozicije kroz sezone, ukupno meceva i pobeda kroz sezone, ukupno meceva i pobeda posmatrano tokom cele karijere,
datum rodjenja igraca.
*/

db.rankings_1973_2017.aggregate([
    {$match:{"rank_number":{$lte:50}}},    //uzimanje u obzir samo onih dokumenata koji se odnose na nedelju tenisera u kojoj je na rang listi bio u prvih 50 pozicija
    {$group:{"_id":{"Godina":"$week_year", "Igrac":"$player_slug"},    //grupisanje po sezoni i po igracu 
        "srednja_vrednost_pozicije":{$avg:"$rank_number"}, 
        "standardna_devijacija":{$stdDevPop:"$rank_number"}}},
    {$sort:{"srednja_vrednost_pozicije":1, "standardna_devijacija":1}},    //sortiranje po najboljim vrednostima u opadajucem redosledu
    //pocetak - racunanje srednje vrednosti pozicije i standardne devijacije, posmatrano zbirno, za celu karijeru    
    {$group:
        {"_id":"$_id.Igrac", 
            "srednja_vrednost_pozicije":{$avg:"$srednja_vrednost_pozicije"},
            "standardna_devijacija":{$avg:"$standardna_devijacija"},
            "sezone":
            {
                $push:
                {
                    "Godina":"$_id.Godina", 
                    "srednja_vrednost_pozicije":"$srednja_vrednost_pozicije", 
                    "standardna_devijacija":"$standardna_devijacija"
                }
            }
        }
    },
    //kraj - racunanje srednje vrednosti pozicije i standardne devijacije, posmatrano zbirno, za celu karijeru 
    {$sort:{"srednja_vrednost_pozicije":1, "standardna_devijacija":1}},
    //pocetak - spajanje sa all_matches_scores da bi se dobili ukupan broj meceva i ukupan broj pobeda po sezonama, takodje i zbirno
    {$lookup:
        {
            from:"all_matches_scores",
            let:{player_id:"$_id"},
            pipeline:
            [
                {$match:
                    {$expr:
                        {$or:
                            [
                                {$eq:["$$player_id", "$winner_slug"]}, {$eq:["$$player_id", "$loser_slug"]}
                            ]
                         }
                     }
                 },
                 {$project:{"_id":0,"winner_slug":1, "loser_slug":1, "loser_games_won":1, "loser_sets_won":1, 
                     "winner_games_won":1, "winner_sets_won":1, "match_id":1, "match_year":{$substr:["$match_id",0,4]}}},
                 {$group:{"_id":"$match_year", "ukupno_meceva":{$sum:1}, "pobednici":{$push:{"winner_slug":"$winner_slug"}}}},
                 {$project:
                     {
                         "_id":1, 
                         "ukupno_meceva":1, 
                         "pobednici":
                         {
                             $filter:
                                 {input:"$pobednici",
                                  as:"pobednik",
                                  cond:{$eq:["$$pobednik.winner_slug", "$$player_id"]}
                                 }
                         }
                     }
                 },
                 {$project:{"_id":1, "ukupno_meceva":1, "ukupno_pobeda":{$size:"$pobednici"}}},
                 {$facet:
                     {
                         "mecevi":[{$project:{"_id":1, "ukupno_meceva":1, "ukupno_pobeda":1}}, {$sort:{"_id":-1}}],
                         //racunanje zbirno 
                         "ukupno_meceva_i_pobeda":
                         [
                         {$group:
                             {"_id":0, "ukupno_meceva":{$sum:"$ukupno_meceva"}, "ukupno_pobeda":{$sum:"$ukupno_pobeda"}}
                             },
                         {$project:{"_id":0}}
                         ]
                     }
                 }
            ],
            as:"mecevi"
        }
     }, //kraj - spajanje sa all_matches_scores da bi se dobili ukupan broj meceva i ukupan broj pobeda po sezonama, takodje i zbirno
     {$project:{"_id":1, "mecevi":1, "sezone":1}},
     // spajanje sa player_overviews da bi se dobili podaci o igracu
     {$lookup:
         {from:"player_overviews",
         let:{player_id:"$_id"},
         pipeline:
         [
             {$match:{$expr:{$eq:["$$player_id", "$player_slug"]}}},
             {$project:{"_id":0, "naziv_igraca":{$concat:["$first_name", " ", "$last_name"]}, "rodjen":"$birth_year"}}
         ],
         as:"karakteristike_igraca"
         }
     }
])
    
    