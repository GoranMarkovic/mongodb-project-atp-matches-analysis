/*
9. Prikazati naziv turnira, podlogu na kojoj se odigrava i lokaciju odigravanja. 
Za svaki turnir prikazati top 5 tenisera koji su ga najviše puta osvajali, sortirano u opadajućem redosledu. 
Takođe, za svaki turnir, prikazati top 5 tenisera koji imaju najviše izgubljenih finala i polufinala.
Za svakog osvajaca prikazati bekend udarac, datum rodjenja, mesto rodjenja, visinu, tezinu i godinu kada je
postao profesionalac.
*/

//db.all_matches_scores.createIndex({"tourney_slug":1})
//db.player_overviews.createIndex({"player_slug":1})

db.tournaments_1877_2017.aggregate
([
    { // odbacivanje dokumenata u tournaments
        $match:
        {
            $expr:
            {
                $and:
                [
                    {$ne:["$singles_winner_player_slug", null]},
                    {$ne:["$tourney_slug", null]}
                ]
            }
        }
    },
    { // grupisanje po turniru i osvajacu
        $group:{"_id":{"turnir":"$tourney_slug", "osvajac":"$singles_winner_player_slug"}, "broj_titula":{$sum:1}, "podloga":{$addToSet:"$tourney_surface"}}
    },
    {  // grupisanje svih osvajaca posmatranog turnira u jedan dokument
        $group:{"_id":"$_id.turnir", "igrac":{$push:{"osvajac":"$_id.osvajac", "broj_titula":"$broj_titula"}}}
    }, // sortiranje elemenata u okviru niza igrac u opadajucem redosledu po broju titula i odabir top 5 - pocetak
    {$unwind:"$igrac"},
    {$sort:{"igrac.broj_titula":-1}},
    {$group:{"_id":"$_id", "podaci":{$push:"$$ROOT"}}},
    {$project:{"turnir":"$_id", "_id":0, "podaci":{$slice:["$podaci",5]}}},
    // sortiranje elemenata u okviru niza igrac u opadajucem redosledu po broju titula i odabir top 5 - kraj
    // pronalazenje top 5 tenisera sa najvise izgubljenih finala i polufinala na posmatranim turnirima - pocetak
    {$project:
        {
           "podaci._id":0 
        }
    },
    {
        $lookup:
        {
            from:"all_matches_scores",
            let:{turnir:"$turnir"},
            pipeline:
            [
                {
                    $match:
                    {
                        $expr:
                        {
                            $and:
                            [
                                {$eq:["$$turnir", "$tourney_slug"]},
                                {$or:
                                    [
                                        {$eq:["$tourney_round_name", "Finals"]},
                                        {$eq:["$tourney_round_name", "Semi-Finals"]}
                                    ]
                                }
                            ]
                        }
                    },
                },
                // grupisanje po rundi turnira (finala/polufinala) i po gubitniku
                {
                    $group:
                    {
                        "_id":{"runda":"$tourney_round_name", "gubitnik":"$loser_slug"}, "ukupno_poraza":{$sum:1}
                    }
                },
                {
                    $project:
                    {
                        "_id":0,
                        "runda":"$_id.runda",
                        "gubitnik":"$_id.gubitnik",
                        "ukupno_poraza":1
                    }
                },
                {$sort:{"runda":-1, "ukupno_poraza":-1}}
            ],
            as:"finala_polufinala"
        },
    },
    { // segmentacija niza finala_polufinala na niz finala i na niz polufinala 
        $project:
        {
            "turnir":1,
            "podaci":1,
            "finala":{$filter:{input:"$finala_polufinala", as:"fp", cond:{$eq:["$$fp.runda", "Finals"]}}},
            "polufinala":{$filter:{input:"$finala_polufinala", as:"fp", cond:{$eq:["$$fp.runda", "Semi-Finals"]}}}
        }
    },
    { // odabir top 5 po najvisem broju izgubljenih finala i polufinala 
        $project:
        {
            "turnir":1,
            "podaci":1,
            "finala":{$slice:["$finala", 5]},
            "polufinala":{$slice:["$polufinala",5]}
        }
    },
    // pronalazenje top 5 tenisera sa najvise izgubljenih finala i polufinala na posmatranim turnirima - kraj
    // spajanje sa player_overviews
    {
        $lookup:
        {
            from:"player_overviews",
            localField:"podaci.igrac.osvajac",
            foreignField:"player_slug",
            as:"karakteristike_osvajaca"
        }
    },
    {
        $project:
        {
            "turnir":1,
            "podaci":1,
            "finala":1,
            "polufinala":1,
            //"karakteristike_igraca._id":0,
            "karakteristike_igraca.backhand":1,
            "karakteristike_igraca.birthdate":1,
            "karakteristike_igraca.player_slug":1,
            "karakteristike_igraca.birthplace":1,
            "karakteristike_igraca.height_cm":1,
            "karakteristike_igraca.turned_pro":1,
            "karakteristike_igraca.weight_kg":1,
        }
    }
])